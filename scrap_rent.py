import urllib.request as urlreq
import urllib.parse as urlparse
import json

token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJyZWFkIl0sImV4cCI6MTYyMTI5MTE0NCwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QVUJMSUMiXSwianRpIjoiMjdiMzgxNWMtODJhYi00MmJjLTgwNGEtZWIxZDI2NDY5YzFmIiwiY2xpZW50X2lkIjoidmpleWVxdXFlMHpiaTBzd2V6Y3RrZWdrcjJ5aHN2Y28ifQ.AP9AixOFDfhx3Pqg1fxLa9eByLJfV08ULcLYjxHLONQ"

def get_average_price(latlog):

    header = {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
    }

    data = dict(
        country="es",
        operation="rent",
        propertyType="premises",
        center=str(latlog[0]) + "," + str(latlog[1]),
        distance=10000,
        locale="es",
        maxItems=50,
        numPage=1
    )


    post_data = urlparse.urlencode(data).encode('utf-8')
    req = urlreq.Request("https://api.idealista.com/3.5/es/search", headers=header)
    r = urlreq.urlopen(req, data=post_data).read()
    result = json.loads(r.decode('utf-8'))

    price_m2 = 0
    max_price = 0
    min_price = 0
    count = 0
    for i in range(len(result["elementList"])):
        if "priceByArea" in result["elementList"][i]:
            # print("El precio/m2 en " + result["elementList"][i]["address"] +" es "+ str(result["elementList"][i]["priceByArea"]))
            price = result["elementList"][i]["price"]
            m2 = result["elementList"][i]["size"]
            price_m2 += price/m2
            count += 1
            # Añadimos precio mínimo
            if i == 0:
                min_price = price/m2
            elif price/m2 < min_price:
                min_price = price/m2
            # Añadimos el precio máximo
            if price/m2 > max_price:
                max_price = price/m2
    # Código para imprimir la respuesta.
    # json_file = json.dumps(result, ensure_ascii=False, indent=4)
    # print(json_file)
    if count == 0:
        count = 1
    return price_m2/count, min_price, max_price


if __name__ == "__main__":
    price_average, min_price, max_price = get_average_price((42.6075172,-8.4714942))
    print("El precio medio por m2 es: " + str(price_average))
    print("El precio máximo por m2 es: " + str(max_price))
    print("El precio mínimo por m2 es: " + str(min_price))