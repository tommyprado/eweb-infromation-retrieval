import json
from geo import *
from scrap_rent import *


cities = list()
local_rate = list()
with open('careers.json') as json_file:
    data = json.load(json_file)
    careers = data["careers"]

# Ordenamos alfabéticamente
for i in range(len(careers)):
    if careers[i]["city"] not in cities:
        cities.append(careers[i]["city"])
        cities = sorted(cities)

# print(careers)
for city in cities:
    latlong = get_lat_long(city)
    av_price, min_price, max_price = get_average_price(latlong)
    x = {
        "city": city,
        "latlong": latlong,
        "average_price": av_price,
        "min_price": min_price,
        "max_price": max_price
    }
    print(x)
    local_rate.append(x)

final = {"rent_prices": local_rate}

with open('rent_prices.json', 'w') as outfile:
    #json_file = json.dumps(final, ensure_ascii=False, indent=4)
    # print(json_file)
    json.dump(final, outfile, ensure_ascii=False, indent=4)
    # outfile.write(json_file)

cities = sorted(cities)

print(cities)