from bs4 import BeautifulSoup
import requests
import json


def main(url, href, careers, json) -> None:

    # url = 'https://notasdecorte.es/'
    # href = 'zona/galicia'
    page = requests.get(url+href, verify=False)
    soup = BeautifulSoup(page.content, 'html.parser')
    next = soup.find('li', class_='next')
    careers += get_careers(careers, soup)
    print(href)

    if next:
        main(url, next.find('a')['href'], careers, json)
    else:
        final = {"careers": careers}
        with open('careers_gal.json', 'w') as outfile:
            json_file = json.dumps(final, ensure_ascii=False, indent=4)
            #print(json_file)
            json.dump(final, outfile, ensure_ascii=False, indent=4)
            #outfile.write(json_file)

def get_careers(carrers, soup):

    carreras = list()
    ciudades = list()
    universidad = list()
    notas = list()
    modalidad = list()
    tipo = list()
    careers = list()

    # Obtención de las Carreras
    un = soup.find_all('span', class_='')
    for i in un:
        carreras.append(i.text)

    # Obteción ciudad
    cd = soup.find_all('div', class_='titul-list-provincia text-right')
    for i in cd:
        ciudades.append(i.text)

    # Obteción universidad
    uni = soup.find_all('div', class_='views-field views-field-field-universidad-id')
    for i in uni:
        universidad.append(i.text)

    # Obtención de nota de corte
    nt = soup.find_all('div', class_='titul-list-nota-corte-nota')
    for i in nt:
        notas.append(i.text)

    # Obtención de la modalidad
    md = soup.find_all('div', class_='titul-list-modalidad text-right')
    for i in md:
        modalidad.append(i.text)

    # Obtención del tipo
    td = soup.find_all('span', {'class': ['label label-primary', 'label label-warning']})
    for i in td:
        tipo.append(i.text)

    for i in range(len(ciudades)):
        x = {
            "city": ciudades[i],
            "university": universidad[i].strip(),
            "grade": notas[i],
            "style": modalidad[i],
            "type": tipo[i],
            "title": carreras[i].replace('\u0303','').replace('\u0301','')
        }
        carrers.append(x)
    return careers


if __name__ == "__main__":
    careers = list()
    # Para Galicia
    main('https://notasdecorte.es/', 'zona/galicia', careers, json)
    #main('https://notasdecorte.es/', 'zona/castilla-y-leon', careers, json)
    # Para España
    #main('https://notasdecorte.es/', 'buscador-de-notas-de-corte?title=&term_node_tid_depth=All', careers, json)
