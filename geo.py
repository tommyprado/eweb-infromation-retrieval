from opencage.geocoder import OpenCageGeocode

key = '5cbc428bdee2449f98c91316a095b20b'


def get_lat_long(city):
    geocoder = OpenCageGeocode(key)
    results = geocoder.geocode(city, no_annotations='1')
    if results and len(results):
        longitude = results[0]['geometry']['lng']
        latitude = results[0]['geometry']['lat']
    else:
        latitude = 0
        longitude = 0
    return latitude, longitude


if __name__ == "__main__":
    result = get_lat_long('Pontevedra')
    print(result)