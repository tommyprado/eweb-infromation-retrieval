import requests
import json
import googlemaps
from googlemaps import convert

api_key = 'AIzaSyCAW96MEYOOVtLS7kKaC3hpABFG5Acpxuc'


def get_average_rating(lat_long):
    url = "/maps/api/place/search/json"
    params = {}
    params["location"] = googlemaps.convert.latlng(lat_long)
    params["type"] = ["bar", "cafe", "restaurant"]
    params["radius"] = 5000
    gmaps = googlemaps.Client(key=api_key)
    places_result = gmaps._request(url, params)
    # print(json.dumps(places_result, ensure_ascii=False, indent=4))
    # print(len(places_result["results"]))
    size = 0
    rating = 0

    for i in range(len(places_result["results"])):
        if "rating" in places_result["results"][i]:
            size += 1
            rating += places_result["results"][i]["rating"]

   # print(rating / size)
    if size == 0:
        size = 1
    return rating/size

if __name__ == "__main__":
    result = get_average_rating("42.6075172, -8.4714942")
    print(result)
