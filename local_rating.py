import json
from geo import *
from places import *


cities = list()
local_rate = list()
with open('careers_gal.json') as json_file:
    data = json.load(json_file)
    careers = data["careers"]

# Ordenamos alfabéticamente
for i in range(len(careers)):
    if careers[i]["city"] not in cities:
        cities.append(careers[i]["city"])
        cities = sorted(cities)

# print(careers)
for city in cities:
    latlong = get_lat_long(city)
    rate = get_average_rating(latlong)
    x = {
        "city": city,
        "latlong": latlong,
        "rate": rate
    }
    print(x)
    local_rate.append(x)

final = {"local_rating": local_rate}

with open('local_rating_gal.json', 'w') as outfile:
    #json_file = json.dumps(final, ensure_ascii=False, indent=4)
    # print(json_file)
    json.dump(final, outfile, ensure_ascii=False, indent=4)
    # outfile.write(json_file)

cities = sorted(cities)

print(cities)